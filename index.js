;( function ($) {
  
  $.datepicker.setDefaults($.datepicker.regional[ "ja" ]);
  $(".datepicker").datepicker();
  
  var $originForm = $('#originForm');
  var $copyTr     = $('#copyTr');
  var $addBtn     = $('.addBtn');
  
  $('.save').on('click', function () {
    var targetId = 'date_' + $(this).data('id');
    var _text    = $('#' + targetId).val();
    if ( _text === '' ) return alert('日付を選んで下さい');
    $('#form_date').val(_text);
    
  });
  
  var num = 2;
  $addBtn.on('click', function () {
    var clone = $($copyTr).clone(true);
    clone.prop('id', '');
    clone.find('.date')
         .prop('id', 'date_' + num)
         .datepicker();
    clone.find('.save')
         .data('id', num);
    clone.appendTo($originForm);
    num++;
  });
  
  $('[class^=date]').on('change', function () {
    //$(this).val();
    $('#form_date').val($(this).val());
    
    var id = $(this).attr('id');
    console.log(id);
    id = id.replace(/date_/g, '');
    $('#form_id').val(id);
  
    $.ajax({
      url:  '/web_dev/03/ajax.php',
      type: 'post',
      dataType: 'json',
      data: {
        id: $('#form_id').val(),
        date: $('#form_date').val()
      }
    }).then(function (data) {
      console.log('OK');
      console.log(data);
    }, function (err) {
      console.log('NG');
      console.log(err);
    });
    
    
  });
  
  $("#ajax").on('click', function () {
    
    var id = $('#form_id');
    var date = $('#form_date');
    
    
    $.ajax({
      url:  '/web_dev/03/ajax.php',
      type: 'post',
      dataType: 'json',
      data: {
        id: id.val(),
        date: date.val()
      }
    }).then(function (data) {
      console.log('OK');
      console.log(data);
    }, function (err) {
      console.log('NG');
      console.log(err);
    });
  });
  
}(window.jQuery));
