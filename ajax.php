<?php

include_once('db_access.php');

$id = isset($_POST['id']) ? $_POST['id'] : '';
$date = isset($_POST['date']) ? $_POST['date'] : '';

$result = array(
    'statue' => (int) false,
    'id' => $id,
    'date' => $date,
);

$rows = execute_sql(
    'SELECT count(*) AS `cnt` FROM `dates` WHERE `id` = ?;',
    array($id)
);

if ( $rows[0]['cnt'] == 0 ) {
    $ret = execute_sql(
        'INSERT INTO `dates` (`id`, `date`, `deleted`) VALUES ( ?,?,? );',
        array($id, $date, false)
        );
} else {
    $ret = execute_sql(
        'UPDATE `dates` SET `date` = ? WHERE `id` = ?;',
        array($date, $id)
        );
}




echo json_encode($result);